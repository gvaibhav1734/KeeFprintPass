package vs.com.keefprintpass;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionSet;
import android.util.Log;
import android.util.TimeUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.slackspace.openkeepass.domain.Entry;
import de.slackspace.openkeepass.domain.Group;

public class MainFragment extends Fragment {
    private static final String TAG = "MainFragment";

    private static final int KDBX_FILE_CODE = 42;
    private View rootView;
    private LinearLayout mMainLinearLayout;
    private RecyclerView mMainRecyclerView;
    private MainRecyclerViewAdapter mMainRecyclerViewAdapter;
    private TextView header;
    public MainFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mMainLinearLayout = rootView.findViewById(R.id.linear_layout_main);
        mMainRecyclerView = rootView.findViewById(R.id.recycler_view_main);
        mMainRecyclerView.setNestedScrollingEnabled(false);
        mMainRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        header = getActivity().findViewById(R.id.text_view_header);

        /* Database paths go into User.databasePath List.
           If empty create Intent to add at least one database.
           If not empty then get the first database from the list and load passwords in it.
         */
        if (User.getInstance().getDatabasePath().isEmpty()) {
            // Ask for file permissions to read kee pass database
            if (Build.VERSION.SDK_INT >= 23) {
                int permissionCheck = ContextCompat.checkSelfPermission(getContext()
                        , Manifest.permission.READ_EXTERNAL_STORAGE);
                Log.d(TAG, String.valueOf(permissionCheck));
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity()
                            , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                }
            }
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, "Pick a kdbx file"), KDBX_FILE_CODE);
        } else {
            GetGroupsAndEntriesTask getEntriesTask = new GetGroupsAndEntriesTask();
            getEntriesTask.execute(User.getInstance().getDatabasePath().get(0),"MasterPassword");
            header.setText(User.getInstance().getDatabaseName());
        }
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case KDBX_FILE_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    File file = new File(uri.getPath());
                    header = getActivity().findViewById(R.id.text_view_header);
                    header.setText(file.getName());
                    User.getInstance().addDatabasePath(file.getAbsolutePath());
                    GetGroupsAndEntriesTask getEntriesTask = new GetGroupsAndEntriesTask();
                    getEntriesTask.execute(file.getAbsolutePath(),"MasterPassword");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public void setupList(List<Group> groups, List<Entry> entries) {
        mMainRecyclerViewAdapter = new MainRecyclerViewAdapter(groups,entries);
        mMainRecyclerView.setAdapter(mMainRecyclerViewAdapter);
    }

    class GetGroupsAndEntriesTask extends AsyncTask<String, Void, Void> {
        List<Group> groups;
        List<Entry> entries;
        ProgressBar progressBar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = getActivity().findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {
            KeePassDatabaseHelper.openDatabase(params[0],params[1]);
            Date time = Calendar.getInstance().getTime();
            Log.d("TIME LOGGER ",time.toString());
            groups = KeePassDatabaseHelper.getTopGroups();
            entries = KeePassDatabaseHelper.getTopEntries();
            time = Calendar.getInstance().getTime();
            Log.d("TIME LOGGER NEW ",time.toString());
            return null;
        }

        protected void onPostExecute(Void result) {
            progressBar.setVisibility(View.INVISIBLE);
            setupList(groups,entries);
        }
    }
    class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder> {
        List<Group> mGroups;
        List<Entry> mEntries;
        int lastPosition = -1;
        public MainRecyclerViewAdapter(List<Group> groups, List<Entry> entries) {
            mGroups=groups;
            mEntries=entries;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView name, groupNoOfEntries,groupNoOfGroups;
            ImageView groupKeyIcon;

            ViewHolder(View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.text_view_group_name);
                //groupUUID = itemView.findViewById(R.id.text_view_group_uuid);
                groupNoOfEntries = itemView.findViewById(R.id.text_view_group_no_of_entries);
                groupNoOfGroups = itemView.findViewById(R.id.text_view_group_no_of_groups);
                groupKeyIcon = itemView.findViewById(R.id.image_view_group_key_icon);
            }
        }


        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater =
                    (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.layout_groups_keys, mMainLinearLayout, false);
            return new MainRecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final MainRecyclerViewAdapter.ViewHolder holder, final int position) {
            if(position<mGroups.size()) {
                holder.name.setText(mGroups.get(position).getName());
                holder.groupNoOfEntries
                        .setText(String.valueOf(mGroups.get(position).getEntries().size()));
                holder.groupNoOfGroups
                        .setText(String.valueOf(mGroups.get(position).getGroups().size()));
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent groupDisplayActivity
                                = new Intent(getActivity(),GroupDisplayActivity.class);
                        groupDisplayActivity.putExtra("Group"
                                ,mGroups.get(holder.getAdapterPosition()).getName());
                        startActivity(groupDisplayActivity);

                    }
                });
                holder.groupKeyIcon.setImageResource(R.drawable.ic_group);
            }else {
                int newPosition = position - mGroups.size();
                holder.name.setText(mEntries.get(newPosition).getTitle());
                holder.groupKeyIcon.setImageResource(R.drawable.ic_key);
            }
            //setAnimation(holder.itemView, position);
        }
//        private void setAnimation(View viewToAnimate, int position)
//        {
//            // If the bound view wasn't previously displayed on screen, it's animated
//            if (position > lastPosition)
//            {
//                //TODO : Slide up animation
//                AnimationSet animatorSet = new AnimationSet(false);
//                Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
//                animatorSet.addAnimation(fadeInAnimation);
//                viewToAnimate.startAnimation(animatorSet);
//                lastPosition = position;
//            }
//        }
//
//        @Override
//        public void onViewDetachedFromWindow(@NonNull MainRecyclerViewAdapter.ViewHolder holder) {
//            super.onViewDetachedFromWindow(holder);
//            mMainRecyclerView.clearAnimation();
//        }

        @Override
        public int getItemCount() {
            return mGroups.size()+mEntries.size();
        }
    }
}