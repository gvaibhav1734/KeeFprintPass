package vs.com.keefprintpass;


import java.util.ArrayList;
import java.util.List;

import de.slackspace.openkeepass.KeePassDatabase;
import de.slackspace.openkeepass.domain.Entry;
import de.slackspace.openkeepass.domain.KeePassFile;

/**
 * Singleton pattern (There can only be one user using the application).
 */
public class User {
    private static User instance = new User();
    private static List<String> databasePath;

    private User(){
        databasePath = new ArrayList<>();
    }

    public String getDatabaseName() {
        return databasePath.get(0).substring(databasePath.get(0).lastIndexOf('/')+1);
    }
    public List<String> getDatabasePath() {
        return databasePath;
    }

    public void addDatabasePath(String databasePath) {
        User.databasePath.add(databasePath);
    }

    public static User getInstance() {
        return instance;
    }
}
