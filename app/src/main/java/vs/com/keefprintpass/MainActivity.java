package vs.com.keefprintpass;

import android.app.LauncherActivity;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import de.slackspace.openkeepass.domain.KeePassFile;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private RelativeLayout mRelativeLayout;
    private FragmentManager fragmentManager;
    private MainFragment mainFragment;
    private SettingsFragment settingsFragment;
    private SyncFragment syncFragment;
    private AboutFragment aboutFragment;
    private LicensesFragment licensesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

        fragmentManager = getSupportFragmentManager();
        mainFragment = new MainFragment();
        addFragment(mainFragment,"Database");
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();
                        if(menuItem.getTitle().equals("Database")){
                            if(mainFragment==null)
                                mainFragment = new MainFragment();
                            addFragment(mainFragment,menuItem.getTitle().toString());
                        }else if(menuItem.getTitle().equals("Settings")){
                            if(settingsFragment==null)
                                settingsFragment = new SettingsFragment();
                            addFragment(settingsFragment,menuItem.getTitle().toString());
                        }else if(menuItem.getTitle().equals("Sync")){
                            if(syncFragment==null)
                                syncFragment = new SyncFragment();
                            addFragment(syncFragment,menuItem.getTitle().toString());
                        }else if(menuItem.getTitle().equals("About")){
                            if(aboutFragment==null)
                                aboutFragment = new AboutFragment();
                            addFragment(aboutFragment,menuItem.getTitle().toString());
                        }else if(menuItem.getTitle().equals("Licenses")){
                            if(licensesFragment==null)
                                licensesFragment = new LicensesFragment();
                            addFragment(licensesFragment,menuItem.getTitle().toString());
                        }
                        Snackbar.make(mDrawerLayout,menuItem.getTitle(),Snackbar.LENGTH_SHORT)
                                .show();

                        return true;
                    }
                });
    }
    public void addFragment(Fragment fragment,String tag){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(fragmentManager.findFragmentByTag(tag)==null) {
            fragmentTransaction.replace(R.id.relative_layout_main,fragment,tag);
            fragmentTransaction.commit();
        }else {
            fragmentTransaction.show(fragmentManager.findFragmentByTag(tag)).commit();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START))
                    mDrawerLayout.closeDrawers();
                else
                    mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerLayout.closeDrawers();
        else
            super.onBackPressed();
    }
}
