package vs.com.keefprintpass;

import java.util.List;

import de.slackspace.openkeepass.KeePassDatabase;
import de.slackspace.openkeepass.domain.Entry;
import de.slackspace.openkeepass.domain.Group;
import de.slackspace.openkeepass.domain.KeePassFile;

/**
 * Helper Class to connect to keepassdatabase and retrieve,edit,write the same.
 */
public class KeePassDatabaseHelper {
    private static KeePassFile database;

    public static void openDatabase(String file, String masterPassword) {
        database = KeePassDatabase.getInstance(file).openDatabase(masterPassword);
    }

    public static List<Entry> getEntries() {
        return database.getEntries();
    }
    public static List<Entry> getEntries(Group group) {
        return group.getEntries();
    }
    public static List<Entry> getTopEntries() {
        return database.getTopEntries();
    }
    public static List<Group> getTopGroups() {
        return database.getTopGroups();
    }
    public static List<Group> getGroups(Group group) {;
        return group.getGroups();
    }
    public static Group getGroupByName(String groupName) {
        return database.getGroupByName(groupName);
    }
    public static void closeDatabase() {
        database=null;
    }
}
