package vs.com.keefprintpass;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.slackspace.openkeepass.domain.Entry;
import de.slackspace.openkeepass.domain.Group;

public class GroupDisplayActivity extends FragmentActivity {
    private final static String TAG = "GroupDisplay";
    private Group group;
    private LinearLayout mLinearLayout;
    private RelativeLayout mRelativeLayout;
    private RecyclerView mMainRecyclerView;
    private MainRecyclerViewAdapter mMainRecyclerViewAdapter;
    private String groupName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_display);
        mLinearLayout=findViewById(R.id.linear_layout_main);
        mLinearLayout.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED :
                        Log.i(TAG+"START",event.toString());
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED :
                        Log.i(TAG+"DRAG",event.toString());
                        break;
                    case DragEvent.ACTION_DROP :
                        Log.i(TAG+"END",event.toString());
                        break;
                }
                return false;
            }
        });
        mRelativeLayout = findViewById(R.id.relative_layout_main);
        mRelativeLayout.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_LOCATION:
                        mRelativeLayout.setTranslationX(event.getX());
                    case DragEvent.ACTION_DRAG_EXITED:
                        v.setVisibility(View.GONE);
                        onBackPressed();
                        break;
                }
                return true;
            }
        });
        mMainRecyclerView = findViewById(R.id.recycler_view_main);
        mMainRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        groupName = getIntent().getExtras().getString("Group");

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitle(groupName);

        GetGroupsAndEntriesTask getGroupsAndEntriesTask = new GetGroupsAndEntriesTask();
        getGroupsAndEntriesTask.execute(group);
    }

    class GetGroupsAndEntriesTask extends AsyncTask<Group, Void, Void> {
        List<Group> groups;
        List<Entry> entries;
        ProgressBar progressBar;

        @Override
        protected Void doInBackground(Group... params) {
            groups = KeePassDatabaseHelper.getGroups(group);
            entries = KeePassDatabaseHelper.getEntries(group);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            group = KeePassDatabaseHelper.getGroupByName(groupName);
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(Void result) {
            progressBar.setVisibility(View.INVISIBLE);
            setupList(groups,entries);
        }
    }
    public void setupList(List<Group> groups, List<Entry> entries) {
        mMainRecyclerViewAdapter = new MainRecyclerViewAdapter(groups,entries);
        mMainRecyclerView.setAdapter(mMainRecyclerViewAdapter);
    }
    class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder> {
        private List<Group> mGroups;
        private List<Entry> mEntries;
        int lastPosition = -1;

        public MainRecyclerViewAdapter(List<Group> groups, List<Entry> entries) {
            mGroups=groups;
            mEntries=entries;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView name, groupNoOfEntries,groupNoOfGroups;
            ImageView groupKeyIcon;

            ViewHolder(View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.text_view_group_name);
                //groupUUID = itemView.findViewById(R.id.text_view_group_uuid);
                groupNoOfEntries = itemView.findViewById(R.id.text_view_group_no_of_entries);
                groupNoOfGroups = itemView.findViewById(R.id.text_view_group_no_of_groups);
                groupKeyIcon = itemView.findViewById(R.id.image_view_group_key_icon);
            }
        }


        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater =
                    (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.layout_groups_keys, mRelativeLayout, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            if(position<mGroups.size()) {
                holder.name.setText(mGroups.get(position).getName());
                holder.groupNoOfEntries
                        .setText(String.valueOf(
                                mGroups.get(position).getEntries().size()));
                holder.groupNoOfGroups
                        .setText(String.valueOf(
                                mGroups.get(position).getGroups().size()));
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent groupDisplayActivity
                                = new Intent(getApplicationContext(),GroupDisplayActivity.class);
                        groupDisplayActivity.putExtra("Group"
                                ,mGroups.get(holder.getAdapterPosition()).getName());
                        startActivity(groupDisplayActivity);

                    }
                });
                holder.groupKeyIcon.setImageResource(R.drawable.ic_group);
            }else {
                int newPosition = position - mGroups.size();
                holder.name.setText(mEntries.get(newPosition).getTitle());
                holder.groupKeyIcon.setImageResource(R.drawable.ic_key);
            }
            setAnimation(holder.itemView, position);
        }
        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }

        @Override
        public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            mMainRecyclerView.clearAnimation();
        }

        @Override
        public int getItemCount() {
            return mGroups.size()+mEntries.size();
        }
    }
}
